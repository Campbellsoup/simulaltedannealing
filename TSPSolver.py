#!/usr/bin/python3

from which_pyqt import PYQT_VER
if PYQT_VER == 'PYQT5':
    from PyQt5.QtCore import QLineF, QPointF
elif PYQT_VER == 'PYQT4':
    from PyQt4.QtCore import QLineF, QPointF
else:
    raise Exception('Unsupported Version of PyQt: {}'.format(PYQT_VER))




import time
import numpy as np
from TSPClasses import *
import heapq
import copy



class TSPSolver:
    def __init__( self, gui_view ):
        self._scenario = None

    def setupWithScenario( self, scenario ):
        self._scenario = scenario


    ''' <summary>
        This is the entry point for the default solver
        which just finds a valid random tour
        </summary>
        <returns>results array for GUI that contains three ints: cost of solution, time spent to find solution, number of solutions found during search (
not counting initial BSSF estimate)</returns> '''
    def defaultRandomTour( self, start_time, time_allowance=60.0 ):

        results = {}


        start_time = time.time()

        cities = self._scenario.getCities()
        ncities = len(cities)
        foundTour = False
        count = 0
        while not foundTour:
            # create a random permutation
            perm = np.random.permutation( ncities )

            #for i in range( ncities ):
                #swap = i
                #while swap == i:
                    #swap = np.random.randint(ncities)
                #temp = perm[i]
                #perm[i] = perm[swap]
                #perm[swap] = temp

            route = []

            # Now build the route using the random permutation
            for i in range( ncities ):
                route.append( cities[ perm[i] ] )

            bssf = TSPSolution(route)
            #bssf_cost = bssf.cost()
            #count++;
            count += 1

            #if costOfBssf() < float('inf'):
            if bssf.costOfRoute() < np.inf:
                # Found a valid route
                foundTour = True
        #} while (costOfBssf() == double.PositiveInfinity);                // until a valid route is found
        #timer.Stop();

        results['cost'] = bssf.costOfRoute() #costOfBssf().ToString();                          // load results array
        results['time'] = time.time() - start_time
        results['count'] = count
        results['soln'] = bssf

       # return results;
        return results


    #O((n^3)/2)) time and O(n) space complexity because I visit every city as a starting point once and then find a greedy solution by always selecting the lowest next cost
    #which takes n^2/2 time.
    def greedy(self, start_time, time_allowance=60.0):
        results = {}

        start_time = time.time()

        cities = self._scenario.getCities()
        ncities = len(cities)
        route = []
        bssf = TSPSolution(route)
        bestCostSoFar = np.inf
        for x in range(0, ncities):  # choose a start city
            citiesCopy = cities[:]
            ncitiesCopy = len(citiesCopy)
            prevCity = citiesCopy[x]
            totalCost = 0
            prevCost = 0
            route = []
            while ncitiesCopy > 0:  # find greediest path by taking shortest next city
                route.append(prevCity)
                citiesCopy.remove(prevCity)  # remove by value
                ncitiesCopy -= 1
                totalCost += prevCost
                tempCity = prevCity
                prevCost = 0
                for y in range(0, ncitiesCopy):  # find shortest next distance to a city
                    nextCity = citiesCopy[y]
                    nextCost = prevCity.costTo(nextCity)
                    if nextCost < prevCost or prevCost == 0:  # keep track of shortest path after checking every city reachable
                        tempCity = nextCity
                        prevCost = nextCost
                prevCity = tempCity
            if bestCostSoFar > totalCost:
                bestCostSoFar = totalCost
                bssf = TSPSolution(route)

        # } while (costOfBssf() == double.PositiveInfinity);                // until a valid route is found
        results['cost'] = bssf.costOfRoute()
        results['time'] = time.time() - start_time
        results['count'] = ncities  # the number of cities in the solution
        results['soln'] = bssf
        return results

    def generous(self, start_time, time_allowance=60.0):
        results = {}

        start_time = time.time()

        cities = self._scenario.getCities()
        ncities = len(cities)
        route = []
        bssf = TSPSolution(route)
        bestCostSoFar = 0
        for x in range(0, ncities):  # choose a start city
            citiesCopy = cities[:]
            ncitiesCopy = len(citiesCopy)
            prevCity = citiesCopy[x]
            totalCost = 0
            prevCost = 0
            route = []
            tempCity = None
            prevTempCity = prevCity
            while ncitiesCopy > 0 and tempCity != prevTempCity:  # find greediest path by taking shortest next city
                prevTempCity = tempCity
                route.append(prevCity)
                citiesCopy.remove(prevCity)  # remove by value
                ncitiesCopy -= 1
                totalCost += prevCost
                tempCity = prevCity
                prevCost = 0
                for y in range(0, ncitiesCopy):  # find shortest next distance to a city
                    nextCity = citiesCopy[y]
                    nextCost = prevCity.costTo(nextCity)
                    if (nextCost > prevCost and nextCost != np.inf) or (prevCost == 0 and nextCost != np.inf):  # keep track of shortest path after checking every city reachable
                        tempCity = nextCity
                        prevCost = nextCost
                prevCity = tempCity
            if ncitiesCopy != 0:
                totalCost = np.inf
                for c in range(0,len(citiesCopy)):
                    route.append(citiesCopy[c])
            if bestCostSoFar < totalCost:
                bestCostSoFar = totalCost
                bssf = TSPSolution(route)

        # } while (costOfBssf() == double.PositiveInfinity);                // until a valid route is found
        results['cost'] = bssf.costOfRoute()
        results['time'] = time.time() - start_time
        results['count'] = ncities  # the number of cities in the solution
        results['soln'] = bssf
        return results

    def simulatedAnnealingGenerous(self, start_time, time_allowance=60.0):
        start_time = time.time()

        generousSolution = self.generous(self, start_time)
        upperBound = generousSolution['cost']
        if upperBound == np.inf:  # if no solution just make random
            generousSolution = self.defaultRandomTour(self, start_time)
            upperBound = generousSolution['cost']

        results = {}
        cities = self._scenario.getCities()
        ncities = len(cities)
        bssf = generousSolution['soln']
        bssf.saveCost = generousSolution['cost']  # Added to TSPSOLUTION class for less computational costs
        solutionCount = 0

        ################Annealing code here#####################
        startingTemperature = 10000
        temperature = startingTemperature
        coolingRate = 0.003
        currentSolution = copy.deepcopy(bssf)
        #Loop until system has cooled
        while (temperature > 1):# Create new neighbour tour
            newSolution = copy.deepcopy(currentSolution)

            # Get a random positions in the tour
            tourPos1 = np.random.randint(0, len(currentSolution.route))
            tourPos2 = np.random.randint(0, len(currentSolution.route))

            # Get the cities at selected positions in the tour
            citySwap1 = newSolution.route[tourPos1]
            citySwap2 = newSolution.route[tourPos2]

            # Swap them
            newSolution.route[tourPos2] = citySwap1
            newSolution.route[tourPos1] = citySwap2

            # Get energy of solutions
            currentEnergy = currentSolution.costOfRoute()
            neighbourEnergy = newSolution.costOfRoute()

            # Decide if we should accept the neighbour
            probability = self.acceptanceProbability(currentEnergy, neighbourEnergy, temperature)
            randomInt = np.random.random()
            if probability > randomInt:
                currentSolution = newSolution

            # Keep track of the best solution found
            if currentSolution.costOfRoute() < bssf.costOfRoute():
                bssf = currentSolution

            # Cool system
            temperature *= 1-coolingRate

        results['cost'] = bssf.costOfRoute()
        results['time'] = time.time() - start_time
        results['count'] = solutionCount  # the number of cities in the solution
        results['soln'] = bssf
        print("%s %i" % ("Starting temperature:", startingTemperature))
        print("%s %f" % ("Cooling Rate:", coolingRate))
        return results

    def simulatedAnnealingRandom(self, start_time, time_allowance=60.0):
        start_time = time.time()

        randomSolution = self.defaultRandomTour(self, start_time)
        upperBound = randomSolution['cost']
        if upperBound == np.inf:  # if no solution just make random
            randomSolution = self.defaultRandomTour(self, start_time)
            upperBound = randomSolution['cost']

        results = {}
        cities = self._scenario.getCities()
        ncities = len(cities)
        bssf = randomSolution['soln']
        bssf.saveCost = randomSolution['cost']  # Added to TSPSOLUTION class for less computational costs
        solutionCount = 0

        ################Annealing code here#####################

        startingTemperature = 10000
        temperature = startingTemperature
        coolingRate = 0.003
        currentSolution = copy.deepcopy(bssf)
        #Loop until system has cooled
        while (temperature > 1):# Create new neighbour tour
            newSolution = copy.deepcopy(currentSolution)

            # Get a random positions in the tour
            tourPos1 = np.random.randint(0, len(currentSolution.route))
            tourPos2 = np.random.randint(0, len(currentSolution.route))

            # Get the cities at selected positions in the tour
            citySwap1 = newSolution.route[tourPos1]
            citySwap2 = newSolution.route[tourPos2]

            # Swap them
            newSolution.route[tourPos2] = citySwap1
            newSolution.route[tourPos1] = citySwap2

            # Get energy of solutions
            currentEnergy = currentSolution.costOfRoute()
            neighbourEnergy = newSolution.costOfRoute()

            # Decide if we should accept the neighbour
            probability = self.acceptanceProbability(currentEnergy, neighbourEnergy, temperature)
            randomInt = np.random.random()
            if probability > randomInt:
                currentSolution = newSolution

            # Keep track of the best solution found
            if currentSolution.costOfRoute() < bssf.costOfRoute():
                bssf = currentSolution

            # Cool system
            temperature *= 1-coolingRate

        results['cost'] = bssf.costOfRoute()
        results['time'] = time.time() - start_time
        results['count'] = solutionCount  # the number of cities in the solution
        results['soln'] = bssf
        print("%s %i" % ("Starting temperature:", startingTemperature))
        print("%s %f" % ("Cooling Rate:", coolingRate))
        return results

    def simulatedAnnealingGreedy(self, start_time, time_allowance=60.0):
        start_time = time.time()

        greedySolution = self.greedy(self, start_time)
        upperBound = greedySolution['cost']
        if upperBound == np.inf:  # if no solution just make random
            greedySolution = self.defaultRandomTour(self, start_time)
            upperBound = greedySolution['cost']

        results = {}
        cities = self._scenario.getCities()
        ncities = len(cities)
        bssf = greedySolution['soln']
        bssf.saveCost = greedySolution['cost']  # Added to TSPSOLUTION class for less computational costs
        solutionCount = 0

        ################Annealing code here#####################

        startingTemperature = 10000
        temperature = startingTemperature
        coolingRate = 0.003
        currentSolution = copy.deepcopy(bssf)
        #Loop until system has cooled
        while (temperature > 1):# Create new neighbour tour
            newSolution = copy.deepcopy(currentSolution)

            # Get a random positions in the tour
            tourPos1 = np.random.randint(0, len(currentSolution.route))
            tourPos2 = np.random.randint(0, len(currentSolution.route))

            # Get the cities at selected positions in the tour
            citySwap1 = newSolution.route[tourPos1]
            citySwap2 = newSolution.route[tourPos2]

            # Swap them
            newSolution.route[tourPos2] = citySwap1
            newSolution.route[tourPos1] = citySwap2

            # Get energy of solutions
            currentEnergy = currentSolution.costOfRoute()
            neighbourEnergy = newSolution.costOfRoute()

            #TODO: we could run several times and take an average
            #TODO: we could run several times and take the highest
            #TODO: we could alter the greedy to start with the worst possible solution
            # Decide if we should accept the neighbour
            probability = self.acceptanceProbability(currentEnergy, neighbourEnergy, temperature)#TODO: almost always returns true in favor of the new energy if it is less, not ideal because we dont search the whole problem space..
            randomInt = np.random.random()
            if probability > randomInt:
                currentSolution = newSolution

            # Keep track of the best solution found
            if currentSolution.costOfRoute() < bssf.costOfRoute():
                bssf = currentSolution

            # Cool system
            temperature *= 1-coolingRate

        results['cost'] = bssf.costOfRoute()
        results['time'] = time.time() - start_time
        results['count'] = solutionCount  # the number of cities in the solution
        results['soln'] = bssf
        print("%s %i" % ("Starting temperature:", startingTemperature))
        print("%s %f" % ("Cooling Rate:", coolingRate))
        return results

    def acceptanceProbability(self, oldEnergy, newEnergy, temperature):
        if newEnergy < oldEnergy:
            return 1
        return np.exp((oldEnergy - newEnergy) / temperature)

    def branchAndBound( self, start_time, time_allowance=60.0 ):

        start_time = time.time()

        greedySolution = self.greedy(self, start_time)
        upperBound = greedySolution['cost']
        if upperBound == np.inf:#if no solution just make random
            greedySolution = self.defaultRandomTour(self, start_time)
            upperBound = greedySolution['cost']

        results = {}
        cities = self._scenario.getCities()
        ncities = len(cities)
        bssf = greedySolution['soln']
        bssf.saveCost = greedySolution['cost']#Added to TSPSOLUTION class for less computational costs
        myHeapQ = []
        heapq.heapify(myHeapQ)
        costMatrix = np.zeros((ncities, ncities), dtype=np.float)
        for y in range(0, ncities):
            for x in range(0, ncities):
                costMatrix[y,x] = cities[y].costTo(cities[x])
        reducedMatrixAndCostTuple = self.reduceMatrix(costMatrix)

        startNode = TSPNode(reducedMatrixAndCostTuple[0], reducedMatrixAndCostTuple[1], [cities[0]._index])
        heapq.heappush(myHeapQ, startNode)

        #Data For GUI
        solutionCount = 0
        numPruned = 0
        childStatesCount = 1
        greatestHeapQSize = 1

        while len(myHeapQ) > 0:
            if (time.time() - start_time) > time_allowance:
                break
            if len(myHeapQ) > greatestHeapQSize:
                greatestHeapQSize = len(myHeapQ)
            top = heapq.heappop(myHeapQ)
            while top.getCost() > bssf.saveCost and len(myHeapQ) > 0:#pop until a cheaper path is found(Prune)
                top = heapq.heappop(myHeapQ)
                numPruned += 1
            if len(top.path) == ncities:
                if top.getCost() < bssf.saveCost:#new best solution so far
                    route = []
                    for x in range(0, len(top.path)):
                        route.append(cities[top.path[x]])
                    bssf = TSPSolution(route)
                    bssf.saveCost = bssf.costOfRoute()
                    solutionCount += 1

            for x in range(0, ncities):#add all branches to queue as TSPNodes
                if top.matrix[top.path[-1]][x] != np.inf:
                    childStatesCount += 1
                    reducedBranchMatrixAndCostTuple = self.reduceMatrix(top.matrix, top.cost, top.path[-1], x)
                    branchUpdatedPath = top.path[:]
                    branchUpdatedPath.append(x)
                    tspNode = TSPNode(reducedBranchMatrixAndCostTuple[0], reducedBranchMatrixAndCostTuple[1], branchUpdatedPath)
                    heapq.heappush(myHeapQ, tspNode)

        results['cost'] = bssf.costOfRoute()
        results['time'] = time.time() - start_time
        results['count'] = solutionCount  # the number of cities in the solution
        results['soln'] = bssf
        print("%s %i" % ("Number of solutions found:", solutionCount))
        print("%s %i" % ("Number of prunes:", numPruned))
        print("%s %i" % ("Number of child states created:", childStatesCount))
        print("%s %i" % ("Greatest heap size:", greatestHeapQSize))
        return results

    def fancy( self, start_time, time_allowance=60.0 ):
        pass

    def reduceMatrix(self, parentMatrix, cost=0, rowToBeInfIndex=None, colToBeInfIndex=None):
        lowerBoundCost = cost
        matrix = np.copy(parentMatrix)
        if rowToBeInfIndex != None and colToBeInfIndex != None:
            lowerBoundCost += matrix[rowToBeInfIndex][colToBeInfIndex]
            matrix[rowToBeInfIndex,:] = np.inf
            matrix[:,colToBeInfIndex] = np.inf
            matrix[colToBeInfIndex][rowToBeInfIndex] = np.inf

        rowMinArray = np.argmin(matrix, axis=1)#reduce rows
        for y in range(0, len(rowMinArray)):
            if matrix[y][rowMinArray[y]] != 0 and matrix[y][rowMinArray[y]] != np.inf:
                incurredCost = matrix[y][rowMinArray[y]]
                matrix[y,:] -= incurredCost
                lowerBoundCost += incurredCost

        colMinArray = np.argmin(matrix, axis=0)#reduce columns
        for x in range(0, len(colMinArray)):
            if matrix[colMinArray[x]][x] != 0 and matrix[colMinArray[x]][x] != np.inf:
                incurredCost = matrix[colMinArray[x]][x]
                matrix[:,x] -= incurredCost
                lowerBoundCost += incurredCost

        return matrix, lowerBoundCost

